import 'package:flutter/material.dart';
import 'package:module3_assignment/main2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: login(),
        theme: ThemeData(
          primarySwatch: Colors.green,
          accentColor: Colors.green,
          scaffoldBackgroundColor: Color.fromARGB(255, 141, 207, 143),
          textTheme: TextTheme(
            bodyText2: TextStyle(color: Colors.white),
          ),
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                  primary: Colors.green, // background (button) color
                  onPrimary: Colors.white)),
        ));
  }
}
